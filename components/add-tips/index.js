const STORAGE_KEY = 'PLUG-ADD-MYAPP-KEY'
Component({
  properties: {
    name: {
      type: String,
      value: '小程序名'
    },
    duration: {
      type: Number,
      value: 5
    },
		delay: {
			type: Number,
			value: 1
		},
    logo: {
      type: String,
      value: '../../images/logo.png'
    },
    custom: {
      type: Boolean,
      value: false
    }
  },
  lifetimes: {
    attached: function() {
      // if (wx.getStorageSync(STORAGE_KEY)) return;
      if (wx.getStorageSync(STORAGE_KEY)){
        let millisecond = new Date().getTime()-wx.getStorageSync(STORAGE_KEY)
        console.log(millisecond)
        if((millisecond/86400000)<7){
          return;
        }else{
          console.log('大于7天了，再次显示')
        }
      };
       
      let rect = wx.getMenuButtonBoundingClientRect ? wx.getMenuButtonBoundingClientRect() : null
      let {screenWidth} = wx.getSystemInfoSync()
      this.setData({
        navbarHeight: rect.bottom,
        arrowR: screenWidth - rect.right + rect.width*3/4 - 5,
        bodyR: screenWidth - rect.right
      })
      this.startTimer = setTimeout(() => {
        this.setData({
          SHOW_TOP: true
        })
      }, this.data.delay * 1000)
      this.duraTimer = setTimeout(() => {
        this.shrink();
      }, (this.data.duration + this.data.delay) * 1000)
    },
    detached: function() {
      if (this.startTimer) clearTimeout(this.startTimer)
      if (this.duraTimer) clearTimeout(this.duraTimer) 
    },
  },
  data: {
    SHOW_TOP: false
  },
  methods: {
    shrink:function() {
      console.log('fasfd23')
      wx.setStorage({
        key:STORAGE_KEY,
        data:new Date().getTime()
      })
      this.animate('#add-tips', [
        {scale: [1, 1]},
        {scale: [0, 0], ease:'ease', transformOrigin: `calc(600rpx - ${this.data.arrowR}px) 1%`}
      ], 500, function () {
        this.setData({
          SHOW_TOP: false
        })
      }.bind(this))
    }
  }
})